package edu.msoe.se2832.dentalscheduler;
public interface iDentalAppointmentScheduler {

	/**
	 * The following indicates that the patient is a regular patient and does
	 * not require any special care.
	 */
	public static final int REGULAR = 0;
	/**
	 * The following indicates this is a Diabetic patient who requires special
	 * dental care.
	 */
	public static final int DIABETIC = 1;

	/**
	 * This indicates that the given patient is high risk and needs special
	 * care.
	 */
	public static final int HIGH_RISK = 2;

	/**
	 * This enumeration defines the sex of the patient.
	 * 
	 * @author schilling
	 *
	 */
	public enum SEX {
		MALE, FEMALE
	}

	/**
	 * This method will set the patient type.
	 * 
	 * @param patientType
	 *            The patient type is either REGULAR, DIABETIC, or HIGH_RISK. If
	 *            the patient type is not one of these types, the default
	 *            patient type will remain unchanged.
	 */
	public void setPatientType(int patientType);
	
	/**
     * This method will set the sex of the patient.
	 * @param sex This is the sex of the patient.
	 */
	public void setPatientSex(SEX sex);

	/**
	 * This method will set the name of the patient.
	 * 
	 * @param patientName
	 *            This is the patent name.
	 * @throws Exception
	 *             An exception will be thrown if the name is invalid. Patient
	 *             name must include at least a first and last name, and may
	 *             include a middle initial. Patient names will be separated
	 *             with whitespace.
	 */
	public void setPatientName(String name) throws Exception;

	/**
	 * This method will obtain the next date for which the insurance company
	 * will allow a patient to have a dental appointment. Regular patients can
	 * be seen every 183 days. Diabetic patients can be seen every 92 days. High
	 * risk patients can be seen every 61 days. Patients who are pregnant can be
	 * seen every 61 days no matter what their risk is.
	 * 
	 * @param lastAppointmentDate
	 *            This is the date of the last appointment in the form
	 *            MM/DD/YYYY HH:MM (i.e. 07/04/1999 17:45) or MM-DD-YYYY HH:MM
	 *            (04-21-2001 09:30) or MMM DD YYYY HH:MM (Jun 12 2001 17:15)
	 *            Appointments will only occur on the quarter hours..
	 * @param pregnant
	 *            This variable will be true if the patient is pregnant and
	 *            false otherwise.
	 * @return This is the first date that the insurance company will allow for
	 *         a dental appointment. It must be a Monday through Friday, as this
	 *         company will not pay convenience charges for Saturday
	 *         appointments. The format of the string will be in the form Name
	 *         WEEKDAY MM/DD/YYYY HH:MM (i.e. George H. Bush Monday, 04/15/2015)
	 * @throws Exception
	 *             An exception will be thrown if the date is either invalid
	 *             (i.e. 13/31/1999) or the string can not be properly parsed.
	 */
	public String obtainNextDentalAppointmentDate(String currentDate,
			boolean pregnant) throws Exception;
}
/**
 * 
 */
package edu.msoe.se2832.dentalscheduler;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * @author boddyn
 *
 */
public class DentalAppointmentScheduler implements iDentalAppointmentScheduler {

    private int patientType;
    private iDentalAppointmentScheduler.SEX patientSex;
    private String[] patientName;

    /**
     * Constructs a DentalAppointmentScheduler
     * @param patientType Must be one of: REGULAR, DIABETIC, HIGH_RISK. Indicates
     *                    the health status of the patient, affecting how often they
     *                    can be seen.
     * @param patientSex Must be one of: MALE, FEMALE. Indicates sex of patient.
     * @param patientName Indicates name of the patient. Patient name must include
     *                    at least a first and last name, and optionally a middle initial.
     *                    Names must be separated by whitespace.
     * @throws Exception An exception for either an invalid patient type or patient name.
     */
    public DentalAppointmentScheduler(int patientType, SEX patientSex, String patientName) throws Exception {
        // error check patient type
        if ((patientType == REGULAR) || (patientType == DIABETIC) || (patientType == HIGH_RISK)) {
            this.patientType = patientType;
        } else {
            throw new Exception("Invalid Patient Type");
        }
        try {
            setPatientSex(patientSex);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setPatientName(patientName);
    }

    /**
     * This method will set the patient type.
     *
     * @param patientType
     *            The patient type is either REGULAR, DIABETIC, or HIGH_RISK. If
     *            the patient type is not one of these types, the default
     *            patient type will remain unchanged.
     */
	@Override
	public void setPatientType(int patientType) {
		if ((patientType >= REGULAR) && (patientType <= HIGH_RISK)) {
			this.patientType = patientType;
		}
	}

    /**
     * This method will set the sex of the patient.
     * @param sex This is the sex of the patient.
     */
	@Override
	public void setPatientSex(SEX sex) {
        if(sex != null) {
            this.patientSex = sex;
        }
	}

    /**
     * Gets the patient's health status.
     * @return One of: REGULAR, DIABETIC, HIGH_RISK.
     */
    public int getPatientType() {
        return patientType;
    }

    /**
     * Gets the sex of the patient.
     * @return One of: MALE, FEMALE.
     */
    public SEX getPatientSex() {
        return patientSex;
    }

    /**
     * Gets the name of the patient.
     * @return A String containing the patient's first and last name, and possibly
     * middle initial, separated by whitespace.
     */
    public String getPatientName() {
        String out = "";
        for (String partialName : patientName) {
            out += partialName + " ";
        }
        return out.trim();
    }

    /**
     * This method will set the name of the patient.
     *
     * @param name
     *            This is the patent name.
     * @throws Exception
     *             An exception will be thrown if the name is invalid. Patient
     *             name must include at least a first and last name, and may
     *             include a middle initial. Patient names will be separated
     *             with whitespace.
     */
	@Override
	public void setPatientName(String name) throws Exception {
        String[] splitName = name.split("\\s+");
        // check if name is either of format John Smith or John T Smith
        if ((splitName.length == 2) || ((splitName.length == 3) && (splitName[1].matches("\\w.?")))) {
            patientName = splitName;
        } else {
            throw new Exception("Invalid name. Patient name must have a first and last, with optional middle initial.");
        }
    }

    /**
     * This method will obtain the next date for which the insurance company
     * will allow a patient to have a dental appointment. Regular patients can
     * be seen every 183 days. Diabetic patients can be seen every 92 days. High
     * risk patients can be seen every 61 days. Patients who are pregnant can be
     * seen every 61 days no matter what their risk is.
     *
     * @param lastAppointmentDate
     *            This is the date of the last appointment in the form
     *            MM/DD/YYYY HH:MM (i.e. 07/04/1999 17:45) or MM-DD-YYYY HH:MM
     *            (04-21-2001 09:30) or MMM DD YYYY HH:MM (Jun 12 2001 17:15)
     *            Appointments will only occur on the quarter hours..
     * @param pregnant
     *            This variable will be true if the patient is pregnant and
     *            false otherwise.
     * @return This is the first date that the insurance company will allow for
     *         a dental appointment. It must be a Monday through Friday, as this
     *         company will not pay convenience charges for Saturday
     *         appointments. The format of the string will be in the form Name
     *         WEEKDAY MM/DD/YYYY HH:MM (i.e. George H. Bush Monday, 04/15/2015)
     * @throws Exception
     *             An exception will be thrown if the date is either invalid
     *             (i.e. 13/31/1999) or the string can not be properly parsed.
     */
	@Override
	public String obtainNextDentalAppointmentDate(String lastAppointmentDate, boolean pregnant) throws Exception {
        // cause that's just silly
        if (pregnant && (patientSex == SEX.MALE)) {
            throw new Exception("Male patients cannot be pregnant");
        }

        // declare temporary storage variables for parsing
        String[] splitLastAppointment, splitTime, splitDate;
        int m, d, y, h, n;

        // Parse that string out
        if (lastAppointmentDate.matches("\\d{1,2} \\d{1,2} (\\d{2}|\\d{4}) \\d{1,2}:\\d{2}")) { // MM DD YYYY HH:MM
            splitLastAppointment = lastAppointmentDate.split("\\s");
            m = Integer.parseInt(splitLastAppointment[0]);
            d = Integer.parseInt(splitLastAppointment[1]);
            y = Integer.parseInt(splitLastAppointment[2]);
            splitTime = splitLastAppointment[3].split(":");
            h = Integer.parseInt(splitTime[0]);
            n = Integer.parseInt(splitTime[1]);
        } else if (lastAppointmentDate.matches("\\d{1,2}/\\d{1,2}/(\\d{2}|\\d{4}) \\d{1,2}:\\d{2}")) { // MM/DD/YYYY HH:MM
            splitLastAppointment = lastAppointmentDate.split("\\s");
            splitDate = splitLastAppointment[0].split("/");
            m = Integer.parseInt(splitDate[0]);
            d = Integer.parseInt(splitDate[1]);
            y = Integer.parseInt(splitDate[2]);
            splitTime = splitLastAppointment[1].split(":");
            h = Integer.parseInt(splitTime[0]);
            n = Integer.parseInt(splitTime[1]);
        } else if (lastAppointmentDate.matches("\\d{1,2}-\\d{1,2}-(\\d{2}|\\d{4}) \\d{1,2}:\\d{2}")) { // MM-DD-YYYY HH:MM
            splitLastAppointment = lastAppointmentDate.split("\\s");
            splitDate = splitLastAppointment[0].split("-");
            m = Integer.parseInt(splitDate[0]);
            d = Integer.parseInt(splitDate[1]);
            y = Integer.parseInt(splitDate[2]);
            splitTime = splitLastAppointment[1].split(":");
            h = Integer.parseInt(splitTime[0]);
            n = Integer.parseInt(splitTime[1]);
        } else if (lastAppointmentDate.matches("[a-zA-Z]+ \\d{1,2} (\\d{2}|\\d{4}) \\d{1,2}:\\d{2}")) { // MMM DD YYYY HH:MM
            splitLastAppointment = lastAppointmentDate.split("\\s");
            m = Month.valueOf(splitLastAppointment[0]).getValue();
            d = Integer.parseInt(splitLastAppointment[1]);
            y = Integer.parseInt(splitLastAppointment[2]);
            splitTime = splitLastAppointment[3].split(":");
            h = Integer.parseInt(splitTime[0]);
            n = Integer.parseInt(splitTime[1]);
        } else throw new Exception("Invalid date format.");

        // convert it to a LocalDateTime and let it do the value checking heavy lifting
        ZonedDateTime lastAppointment;
        try {
            lastAppointment = ZonedDateTime.of(y, m, d, h, n, 0, 0, ZoneId.of("America/Chicago"));
        } catch (DateTimeException dte) {
            throw new Exception("Invalid date value", dte);
        }

        // quarter hours check
        if ((lastAppointment.getMinute() % 15) != 0) {
            throw new Exception("Appointments are only scheduled on quarter hours");
        }

        // logic out how many days till next appointment
        int minDays;
        if ((this.patientType == HIGH_RISK) || pregnant) {
            minDays = 61;
        } else if (this.patientType == DIABETIC) {
            minDays = 92;
        } else if (this.patientType == REGULAR) {
            minDays = 183;
        } else throw new Exception("Something is seriously wrong! Faulty Patient Type");

        // Jump forward in time and make sure its a weekday
        ZonedDateTime nextPossibleAppointment = lastAppointment.plusDays(minDays);
        if (nextPossibleAppointment.getDayOfWeek() == DayOfWeek.SATURDAY) {
            nextPossibleAppointment = nextPossibleAppointment.plusDays(2);
        } else if (nextPossibleAppointment.getDayOfWeek() == DayOfWeek.SUNDAY) {
            nextPossibleAppointment = nextPossibleAppointment.plusDays(1);
        }

        // Format output and return string
        // Name WEEKDAY MM/DD/YYYY HH:MM
        return getPatientName() + " "
                + nextPossibleAppointment
                .format(DateTimeFormatter
                        .ofPattern("EEEE MM/dd/yyyy HH:mm"));
	}

}

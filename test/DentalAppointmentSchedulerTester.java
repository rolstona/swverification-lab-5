package test;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import edu.msoe.se2832.dentalscheduler.*;
import edu.msoe.se2832.dentalscheduler.iDentalAppointmentScheduler.SEX;

/**
 * Test suite for verifying functionality of DentalAppointmentScheduler
 * @author boddyn (Nick Boddy)
 * @version 16.4.15.1
 */
public class DentalAppointmentSchedulerTester {
	
	/**
	 * Creates data of patient types
	 * @return patient type, expected valid
	 */
	@DataProvider (name = "patientTypes")
	public Object[][] createPatientTypes() {
		return new Object[][] {
				{iDentalAppointmentScheduler.REGULAR, true},
				{iDentalAppointmentScheduler.DIABETIC, true},
				{iDentalAppointmentScheduler.HIGH_RISK, true},
				{-1, false},
				{3, false},
				//{null, false}
		};
	}
	
	/**
	 * Tests the setPatientType method (and getPatientType method)
	 * @param patientType
	 * @param expectedValid
	 */
	@Test (dataProvider = "patientTypes")
	public void verifySetPatientType(int patientType, boolean expectedValid) {
		int previousPatientType = iDentalAppointmentScheduler.REGULAR;
		DentalAppointmentScheduler sched;
		try {
			sched = new DentalAppointmentScheduler(previousPatientType, SEX.MALE, "Stan Dard");
			sched.setPatientType(patientType);
			if(expectedValid) {
				Assert.assertEquals(sched.getPatientType(), patientType);
			} else {
				Assert.assertEquals(sched.getPatientType(), previousPatientType);
			}
		} catch (Exception e) {
			Assert.fail();
		}
		//Assert.fail();
	}
	
	/**
	 * Creates data of patient sexes
	 * @return patient sex, expected valid
	 */
	@DataProvider (name = "patientSexes")
	public Object[][] createPatientSexes() {
		return new Object[][] {
				{SEX.MALE, true},
				{SEX.FEMALE, true},
				{null, false}
		};
	}
	
	/**
	 * Tests the setPatientSex method (and getPatientSex method)
	 * @param patientSex
	 * @param expectedValid
	 */
	@Test (dataProvider = "patientSexes")
	public void verifySetPatientSex(iDentalAppointmentScheduler.SEX patientSex, boolean expectedValid) {
		iDentalAppointmentScheduler.SEX previousPatientSex = SEX.MALE;
		DentalAppointmentScheduler sched;
		try {
			sched = new DentalAppointmentScheduler(iDentalAppointmentScheduler.REGULAR, previousPatientSex, "Stan Dard");
			sched.setPatientSex(patientSex);
			if(expectedValid) {
				Assert.assertEquals(sched.getPatientSex(), patientSex);
			} else {
				Assert.assertEquals(sched.getPatientSex(), previousPatientSex);
			}
		} catch (Exception e) {
			Assert.fail();
		}
        //Assert.fail();
	}
	
	/**
	 * Creates data of patient names
	 * @return patient name, expected valid
	 */
	@DataProvider (name = "patientNames")
	public Object[][] createPatientNames() {
		return new Object[][] {
				{"A C", true},
				{"A B C", true},
				{"A    C", true},
				{"A    B  C", true},
				{null, false},
				{"", false},
				{"Addam", false},
				{"12$' 56=%9", true}
		};
	}
	
	/**
	 * Tests the setPatientName method (and getPatientName method)
	 * @param patientName
	 * @param expectedValid
	 */
	@Test (dataProvider = "patientNames")
	public void verifySetPatientName(String patientName, boolean expectedValid) {
		String previousPatientName = "Stan Dard";
		DentalAppointmentScheduler sched;
		try {
			sched = new DentalAppointmentScheduler(iDentalAppointmentScheduler.REGULAR, SEX.MALE, previousPatientName);
			sched.setPatientName(patientName);
			if(expectedValid) {
				Assert.assertEquals(sched.getPatientName(), patientName);
			} else {
				Assert.assertEquals(sched.getPatientName(), previousPatientName);
			}
		} catch(Exception e) {
			Assert.fail();
		}
		//Assert.fail();
	}
	
	/**
	 * Creates data of constructor data
	 * @return patient type, patient sex, patient name, expected valid
	 */
	@DataProvider (name = "constructorData")
	public Object[][] createConstructorData() {
		return new Object[][] {
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "A B C", true},
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "A C", true},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, "A   C", true},
				{-1, SEX.FEMALE, "A B C", false},
				{3, SEX.MALE, "A C", false},
				{null, SEX.MALE, "A B C", false},
				{iDentalAppointmentScheduler.REGULAR, null, "A B C", false},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "A B        C", true},
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "", false},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, null, false},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "  A  ", false},
				{iDentalAppointmentScheduler.DIABETIC, SEX.MALE, "4^j 68%", true}
		};
	}
	
	/**
	 * Tests the DentalAppointmentScheduler constructor
	 * @param patientType
	 * @param patientSex
	 * @param patientName
	 * @param expectedValid
	 */
	@Test (dataProvider = "constructorData")
	public void verifyDentalAppointmentScheduler(int patientType, SEX patientSex, String patientName, boolean expectedValid) {
		boolean isValid = true;
		try {
			DentalAppointmentScheduler sched = new DentalAppointmentScheduler(patientType, patientSex, patientName);
		} catch(Exception e) {
			isValid = false;
		}
		Assert.assertEquals(isValid, expectedValid);
	}
	
	/**
	 * Creates data of dental appointments
	 * @return patient type, patient sex, last appointment date, pregnant, expected appointment date (null indicates invalidity)
	 */
	@DataProvider (name = "dentalAppointments")
	public Object[][] createDentalAppointments() {
		return new Object[][] {
				// Test month boundaries
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "00/07/1994 01:30", false, null},
				// Test across leap year
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "01/07/1996 01:30", false, "Monday 07/10/1996 00:30"},
				// Test enter new year
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "12/07/1994 01:30", false, "Thursday 06/08/1995 00:30"},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "13/07/1994 01:30", false, null},
				// Test the 29th of Feb.
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "02/29/1995 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "02/29/1996 01:30", false, "Friday 08/30/1996 00:30"},
				
				// Test invalid month inputs
				{iDentalAppointmentScheduler.DIABETIC, SEX.MALE, "AB/07/1994 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "A/07/1994 01:30", false, null},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, "001/07/1994 01:30", false, null},
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "1/07/1994 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "/07/1994 01:30", false, null},
				
				// Test day boundaries
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/00/1994 01:30", false, null},
				// Test enter new millenium
				// Test diabetic patient
				// Test dividers of "-"
				{iDentalAppointmentScheduler.DIABETIC, SEX.MALE, "11-01-1999 03:45", false, "Tuesday 02/01/2000 03:45"},
				// Test high risk patient
				// Test MMM DD YYYY HH:MM
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.FEMALE, "Aug 30 2002 01:30", false, "Wednesday 10/30/2002 02:30"},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/32/1994 01:30", false, null},
				
				// Test invalid day inputs
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.FEMALE, "10/AB/1994 01:30", false, null},
				{iDentalAppointmentScheduler.DIABETIC, SEX.MALE, "10/A/1994 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/001/1994 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/1/1994 01:30", false, null},
				{iDentalAppointmentScheduler.DIABETIC, SEX.MALE, "10//1994 01:30", false, null},
				
				// Test year boundaries
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "01/07/-0000 01:30", false, null},
				// Test pregnant patient
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "04/09/0001 01:30", true, "Friday 06/09/0001 01:30"},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "02/20/9999 15:15", false, "Monday 08/23/9999 16:15"},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.FEMALE, "01/07/10000 01:30", false, null},
				
				// Test invalid year inputs
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/ABCD 01:30", false, null},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, "10/01/A 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/00001 01:30", false, null},
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "10/01/001 01:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/ 01:30", false, null},
				
				// Test hour boundaries
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 -01:30", false, null},
				// Test pregnant diabetic patient
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "09/29/2010 12:45", true, "Monday 11/29/2010 11:45"},
				// Test pregnant high risk patient
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.FEMALE, "Oct 01 1994 23:30", true, "Thursday 12/01/1994 22:30"},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 24:30", false, null},
				
				// Test invalid hour inputs
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.FEMALE, "10/01/1994 AB:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 A:30", false, null},
				{iDentalAppointmentScheduler.DIABETIC, SEX.FEMALE, "10/01/1994 001:30", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 1:30", false, null},
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, "10/01/1994 :30", false, null},
				
				// Test minute boundaries
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 01:-01", false, null},
				// Test forward daylight savings
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "03/07/2015 01:00", false, "Monday 09/07/2015 02:00"},
				// Test backward daylight savings
				{iDentalAppointmentScheduler.HIGH_RISK, SEX.MALE, "10/31/2015 01:00", false, "Thursday 12/31/2015 00:00"},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 01:60", false, null},
				
				// Test invalid minutes inputs
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 01:AB", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 01:A", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:015", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "10/01/1994 01:3", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:", false, null},
				
				// Test pregnant male patient (should be invalid (?))
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "05/09/2001 01:30", true, null},
				
				// Test null lastAppointmentDate
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, null, false, null},
				
				// Test empty lastAppointmentDate
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "", false, null},
				
				// Test null pregnancy
				//{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "05/09/2001 01:30", null, null},
				
				// Test jump from year 9999 (should produce invalid date (?), if not, "Tuesday 06/20/10000 14:15"
				{iDentalAppointmentScheduler.REGULAR, SEX.MALE, "12/20/9999 15:15", false, null},
				
				// Test only quarter hours
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:01", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:14", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:16", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:29", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:31", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:44", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:46", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:59", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:10", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:20", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:40", false, null},
				{iDentalAppointmentScheduler.REGULAR, SEX.FEMALE, "10/01/1994 01:50", false, null}
		};
	}
	
	/**
	 * Tests the obtainNextDentalAppointmentDate method
	 * @param patientType
	 * @param patientSex
	 * @param lastAppointmentDate
	 * @param pregnant
	 * @param expectedDate
	 */
	@Test (dataProvider = "dentalAppointments")
	public void verifyObtainNextDentalAppointmentDate(int patientType, SEX patientSex, String lastAppointmentDate, boolean pregnant, String expectedDate) {
		String patientName = "Jordan Smith";
		try {
			DentalAppointmentScheduler sched = new DentalAppointmentScheduler(patientType, patientSex, patientName);
			String appointment = sched.obtainNextDentalAppointmentDate(lastAppointmentDate, pregnant);
			Assert.assertEquals(appointment, patientName + " " + expectedDate);
		} catch(Exception e) {
            e.printStackTrace();
			Assert.assertNull(expectedDate);
		}

	}
	
}
